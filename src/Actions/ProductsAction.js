import { FETCH_PRODUTCS_RECEIVE } from "./ActionTypes";

export const receiveProducts = (data) => async (dispatch) => {
  dispatch({
    type: FETCH_PRODUTCS_RECEIVE,
    value: {
      products: data.products,
      poc: data.id,
    },
  });
};
