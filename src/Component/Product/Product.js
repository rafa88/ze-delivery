import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addProducts, removeProducts } from "../../Actions/CartAction";

import { ProductImage } from "./ProductImage";
import "./Product.css";

const Product = ({ product }) => {
  const [amount, setAmount] = useState(0);
  const dispatch = useDispatch();

  const add = (price) => {
    const count = amount + 1;
    dispatch(addProducts(price));
    setAmount(count);
  };

  const remove = (price) => {
    if (amount === 0) return false;
    const count = amount - 1;
    dispatch(removeProducts(price));
    setAmount(count);
  };

  return (
    <div className="product">
      <p className="product-title">{product.productVariants[0].title}</p>
      <ProductImage
        image={product.images[0].url}
        productName={product.productVariants[0].title}
      />

      <p className="product-price">
        {product.productVariants[0].price.toLocaleString("pt-BR", {
          minimumFractionDigits: 2,
          style: "currency",
          currency: "BRL",
        })}
      </p>
      <div className="product-amount">
        <button
          onClick={() => remove(product.productVariants[0].price)}
          className="product-amount-btn red-btn display-flex justify-content-center align-items-center"
        >
          -
        </button>
        <div className="product-amount-input">
          <p className="text-input">{amount}</p>
        </div>
        <button
          onClick={() => add(product.productVariants[0].price)}
          className="product-amount-btn blue-btn display-flex justify-content-center align-items-center"
        >
          +
        </button>
      </div>
    </div>
  );
};

export default Product;
