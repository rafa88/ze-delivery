import React from 'react';

const styles = {
  minHeight: '150px'
};

export const ProductImage = ({productName, image}) => {
  return (
    <div style={styles}>
      <img src={image} className="product-image" alt={productName} />
    </div>
  );
}