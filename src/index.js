import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route  } from 'react-router-dom'
import { Provider } from 'react-redux';
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import * as serviceWorker from './serviceWorker';

import Home from './Screen/Home/Home';
import Products from './Screen/Products/Products';
import { store } from './store';

import './index.css';

const client = new ApolloClient({
  uri: "https://api.code-challenge.ze.delivery/public/graphql",
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Switch>
      <ApolloProvider client={client}>
        <Route path="/" exact={true} component={Home} />
        <Route path="/produtos/:id" component={Products} />
      </ApolloProvider>
      </Switch>
    </Provider>
  </BrowserRouter>, document.getElementById('root'));

serviceWorker.unregister();
