import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { useLazyQuery } from "@apollo/client";

import Header from "../../Component/Header/Header";
import Product from "../../Component/Product/Product";
import Spinner from "../../Component/Spinner/Spinner";
import SearchProducts from "./SearchProducts";

import { receiveProducts } from "../../Actions/ProductsAction";

import { GET_PRODUCTS } from "../../queries";
import "./Products.css";

const Products = ({ match }) => {
  const dispatch = useDispatch();

  const productsList = useSelector((state) => state.products.products);
  const address = useSelector((state) => state.address);
  const total = useSelector((state) => state.cart);

  const [productsSearch, { loading, data }] = useLazyQuery(GET_PRODUCTS, {
    onCompleted: () => {
      dispatch(receiveProducts(data.poc));
    },
  });

  useEffect(() => {
    productsSearch({
      variables: {
        id: match.params.id,
      },
    });
  }, [productsSearch, match]);

  return (
    <div className="App">
      {loading && <Spinner />}
      <Header address={address} total={total} />

      <SearchProducts productsSearch={productsSearch} />

      <section className="center-page products-list">
        {productsList.map(function (prod, index) {
          return <Product key={index} product={prod} />;
        })}
      </section>
    </div>
  );
};

export default withRouter(Products);
